var i = 0;

var url = [ "https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Lorris",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Malesherbes",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Meung-sur-Loire",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Montargis",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Olivet",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orl%C3%A9ans-1",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orleans2" ]

var timer = 5;
var liste = document.getElementById("select");
var s = document.getElementById("multiple");
var formulaire = document.getElementById("formulaire");
formulaire.style.display = 'none';


function toggleForm(){
    // on réccupère l'élément form.
  
    // Condition pour afficher/cacher le formulaire en fonction de son état
    if(formulaire.style.display == 'block'){
        formulaire.style.display = 'none';
    }else{
        formulaire.style.display = 'block';
    }
}

// Création d'options lorsque l'url correspond aux url par défaut
// Création d'une liste (txt) pour récupérer les url
txt = [];
var e;
for (e = 0; e < liste.options.length; e++) {
    if(url.includes(liste.options[e].value)) {
        s.options[s.options.length] = new Option(liste.options[e].text,liste.options[e].value);
        txt.push(liste.options[e].value);
    }
}

function sortOptions() {

    //Fonction permettant de trier les options sélectionnées dans l'ordre alphabétique

    var options = document.getElementById("multiple").options;
    var optionsArray = [];
    for (var i = 0; i < options.length; i++) {
        optionsArray.push(options[i]);
    }
    optionsArray = optionsArray.sort(function (a, b) {           
        return a.innerHTML.toLowerCase().charCodeAt(0) - b.innerHTML.toLowerCase().charCodeAt(0);    
    });

    for (var i = 0; i <= options.length; i++) {            
        options[i] = optionsArray[i];
    }
    options[0].selected = true;
}


function addCanton () {
    // on récupère la valeur et le texte de l'option sélectionnée
    var text = liste.options[liste.selectedIndex].text;
    var value= liste.options[liste.selectedIndex].value;

    // on vérifie si l'url que l'on veut ajouter existe ou non dans la liste des urls 
    // si elle existe on informe qu'elle se trouve déjà dans la liste
    if (txt.includes(value)) {
        alert("Ce canton est déjà dans la liste");
    }

    // Sinon on créer une option, on ajoute l'url à la liste d'url qui défile

    else {
        s.options[s.options.length] = new Option(text,value);
        txt.push(value);
        url.push(value);
        txt.sort();
        sortOptions();
    }
}

function delCanton () {

    // enregistrer les options selectionnées
    var value= liste.options[liste.selectedIndex].value;

    // supprimer toutes les options selectionnées
    if (txt.includes(value)) {
        let index = s.options.length;
        while (index--) {
            if (s.options[index].value === value) {
                s.remove(index);
                txt.splice(index,1)
            }
        }
    }
    else {
        alert("Ce canton n'existe pas dans la liste")
    }
}

function changeURL () {

    //Fonction défilement des cantons dans chaque frame

    document.getElementById("Montargis").src = s.options[i].value;

    if (i < s.options.length - 1) {
        i++;
    }else {
        i = 0;
    }
    setTimeout(changeURL,timer*1000);
}

function changeTimer () {

    //Fonction changer le temps de défilement de chaque frame

    var nombre = prompt("Entrer un nombre (en secondes)");
    timer = nombre;
}



window.onload = changeURL

