var i = 0
var url = ["https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#canton=Beaugency",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#canton=Ch%C3%A2lette-sur-Loing",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#canton=Ch%C3%A2teauneuf-sur-Loire",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Courtenay",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Fleury-les-Aubrais",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Gien",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=La%20Fert%C3%A9-Saint-Aubin",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Lorris",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Malesherbes",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Meung-sur-Loire",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Montargis",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Olivet",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orl%C3%A9ans-1",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orleans2",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orleans3",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Orleans4",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Pithiviers",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Saint-Jean-de-Braye",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Saint-Jean-de-la-Ruelle",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Saint-Jean-le-Blanc",
"https://loiret.maps.arcgis.com/apps/dashboards/ee89b78ca0fc4c98ae1d254185018410#Canton=Sully-sur-Loire"]
var timer = 5
var formulaire = document.getElementById("formulaire");
formulaire.style.display = 'none';


function toggleForm(){
    // on réccupère l'élément form.
  
    // Condition pour afficher/cacher le formulaire en fonction de son état
    if(formulaire.style.display == 'block'){
        formulaire.style.display = 'none';
    }else{
        formulaire.style.display = 'block';
    }
}

function changeURL () {

    //Fonction défilement des cantons dans chaque frame
    document.getElementById("frame_one").src = url[i];
    
    if (i < url.length - 1) {
        i++;
    }else {
        i = 0;
    }
    
    attente = setTimeout(changeURL,timer*1000)

}



function changeTimer () {

    //Fonction changer le temps de défilement de chaque frame

    var nombre = prompt("Entrer un nombre (en secondes)");
    timer = nombre;
}

window.onload = changeURL
